import socket
from Convert import convert

TCP_IP = '127.0.0.1'
TCP_PORT = 1234
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print('Connection address:', addr)
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    number = data.decode()
    result = convert(number)
    print("received data:", number)
    conn.send(result.encode())  # echo
conn.close()