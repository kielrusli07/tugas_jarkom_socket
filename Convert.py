numbers = ["nol","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan"]

def convert(number):
	print(number)
	if len(number) == 1:
		return(numbers[int(number)])
	elif len(number) == 2:
		if number[0] == '1':
			if number[1] == '0':
				return("sepuluh")
			elif number[1] == "1":
				return("sebelas")
			else:
				return(numbers[int(number[1])]+" belas")
		elif number[1] == '0':
			return(numbers[int(number[0])] + " puluh")
		else:
			return(numbers[int(number[0])] + " puluh " + numbers[int(number[1])])
	elif len(number) == 3:
		if number[0] == '1':
			if number[1] == "0" and number[2] == '0':
				return("seratus")
			elif number[1] == '0' and number[2] != '0':
				return("seratus " + numbers[int(number[2])])
			else:
				if number[1] == '1':
					if number[2] == '0':
						return("seratus sepuluh")
					elif number[2] == "1":
						return("seratus sebelas")
					else:
						return("seratus " + numbers[int(number[2])]+" belas")
				elif number[2] == '0':
					return("seratus " + numbers[int(number[1])] + " puluh")
				else:
					return("seratus " + numbers[int(number[1])] + " puluh " + numbers[int(number[2])])
		else:
			if number[1] == "0" and number[2] == '0':
				return(numbers[int(number[0])] + " ratus")  
			elif number[1] == '0' and number[2] != '0':
				return(numbers[int(number[0])] + " ratus " + numbers[int(number[2])])
			else:
				if number[1] == '1':
					if number[2] == '0':
						return(numbers[int(number[0])] + " ratus sepuluh")
					elif number[2] == "1":
						return(numbers[int(number[0])] + " ratus sebelas")
					else:
						return(numbers[int(number[0])] + " ratus " + numbers[int(number[2])]+" belas")
				elif number[2] == '0':
					return(numbers[int(number[0])] + " ratus " + numbers[int(number[1])] + " puluh")
				else:
					return(numbers[int(number[0])] + " ratus " + numbers[int(number[1])] + " puluh " + numbers[int(number[2])])
	elif number == '1000':
		return("seribu")

# print(convert(input("angka")))